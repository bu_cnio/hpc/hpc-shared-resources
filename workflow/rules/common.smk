def get_resource(rule,resource):
    try:
        return config["resources"][rule][resource]
    except KeyError:
        return config["resources"]["default"][resource]

def update_env(name):
    """updates a conda environment to the latest version of the package"""
    envf = f"workflow/envs/{name}.yaml"

    first = subprocess.Popen(shlex.split(f"mamba search --override-channels -c bioconda -f {name}"), stdout=subprocess.PIPE)
    second = subprocess.Popen(shlex.split("tail -1"), stdin=first.stdout, stdout=subprocess.PIPE)
    third = subprocess.Popen(shlex.split("tr -s ' '"), stdin=second.stdout, stdout=subprocess.PIPE)
    ver = subprocess.check_output(shlex.split("cut -d' ' -f2"), stdin=third.stdout)
    first.wait()
    second.wait()
    third.wait()
    ver = ver.decode().rstrip("\n")
    try:
        config["versions"][name] = ver
    except KeyError:
        config["versions"] = {name:ver}

    with open(envf) as fh:
        data = yaml.load(fh,Loader=yaml.SafeLoader)

    data["dependencies"] = [f"{name}=={ver}"]

    with open(envf,"w") as fh:
        yaml.dump(data, fh)
